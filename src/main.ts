import { bootstrapApplication, provideProtractorTestingSupport } from '@angular/platform-browser';
import { AppComponent } from './app/app.component';
import { provideRouter, Routes } from '@angular/router';
import routeConfig from './app/routes';
import { provideStore } from '@ngrx/store';
bootstrapApplication(AppComponent,
  {
    providers: [
    provideProtractorTestingSupport(),
    provideRouter(routeConfig as Routes),
    provideStore()
]
  }
).catch(err => console.error(err));